<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;

class ReverseScope implements Scope
{
    public function apply($builder, $model)
    {
        $builder->orderBy('id', 'desc');
    }
}
