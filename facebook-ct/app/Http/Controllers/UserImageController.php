<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserImage;
use Intervention\Image\Facades\Image;

class UserImageController extends Controller
{
    public function store()
    {
        $data = request()->validate([
            'image' => 'required',
            'width' => 'required|integer',
            'height' => 'required|integer',
            'location' => 'required',
        ]);

        $image = $data['image']->store('user-images', 'public');

        Image::make($data['image'])->fit($data['width'], $data['height'])->save(storage_path('app/public/user-images/' . $data['image']->hashName()));

        $userImage = auth()->user()->images()->create(
            [
                'path' => $image,
                'width' => $data['width'],
                'height' => $data['height'],
                'location' => $data['location'],
            ]
        );
        return new UserImage($userImage);
    }
}
