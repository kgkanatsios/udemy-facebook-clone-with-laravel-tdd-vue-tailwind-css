<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Resources\CommentCollection;

class PostCommentController extends Controller
{
    public function store(Post $post)
    {
        $data = request()->validate([
            'body' => 'required',
        ]);

        $data['user_id'] = auth()->user()->id;

        $post->comments()->create($data);

        return new CommentCollection($post->comments);
    }
}
