<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostCollection;
use App\User;
use Illuminate\Http\Request;

class UserPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return new PostCollection($user->posts);
    }
}
