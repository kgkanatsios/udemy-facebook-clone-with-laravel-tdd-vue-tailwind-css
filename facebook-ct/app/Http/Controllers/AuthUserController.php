<?php

namespace App\Http\Controllers;

use App\Http\Resources\User as ResourcesUser;
use Illuminate\Http\Request;

class AuthUserController extends Controller
{
    public function show()
    {
        return new ResourcesUser(auth()->user());
    }
}
