<?php

namespace Tests\Feature;

use App\User;
use App\UserImage;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserImagesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
    }

    /** @test */
    public function imagesCanBeUploaded()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($user = factory(User::class)->create(), 'api');

        $file = UploadedFile::fake()->image('user-image.jpg');

        $response = $this->post('/api/user-images', [
            'image' => $file,
            'width' => 850,
            'height' => 300,
            'location' => 'cover',
        ])->assertStatus(201);

        Storage::disk('public')->assertExists('user-images/' . $file->hashName());
        $userImages = UserImage::first();
        $this->assertEquals('user-images/' . $file->hashName(), $userImages->path);
        $this->assertEquals('850', $userImages->width);
        $this->assertEquals('300', $userImages->height);
        $this->assertEquals('cover', $userImages->location);
        $this->assertEquals($user->id, $userImages->user_id);

        $response->assertJson([
            'data' => [
                'type' => 'user-images',
                'user_image_id' => $userImages->id,
                'attributes' => [
                    'path' => asset('storage/' . $userImages->path),
                    'width' => $userImages->width,
                    'height' => $userImages->height,
                    'location' => $userImages->location,
                ],
            ],
            'links' => [
                'self' => url('/users/' . $userImages->user_id)
            ]
        ]);
    }

    /** @test */
    public function usersAreReturnedWithImages()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($user = factory(User::class)->create(), 'api');

        $file = UploadedFile::fake()->image('user-image.jpg');
        $this->post('/api/user-images', [
            'image' => $file,
            'width' => 850,
            'height' => 300,
            'location' => 'cover',
        ])->assertStatus(201);

        $this->post('/api/user-images', [
            'image' => $file,
            'width' => 850,
            'height' => 300,
            'location' => 'profile',
        ])->assertStatus(201);

        $userCoverImage = $user->coverImage;
        $userProfileImage = $user->profileImage;

        $this->get('/api/users/' . $user->id)->assertStatus(200)->assertJson([
            'data' => [
                'type' => 'users',
                'user_id' => $user->id,
                'attributes' => [
                    'cover_image' => [
                        'data' => [
                            'type' => 'user-images',
                            'user_image_id' => $userCoverImage->id,
                            'attributes' => [
                                'path' => asset('storage/' . $userCoverImage->path),
                                'width' => $userCoverImage->width,
                                'height' => $userCoverImage->height,
                                'location' => $userCoverImage->location,
                            ],
                        ],
                    ],
                    'profile_image' => [
                        'data' => [
                            'type' => 'user-images',
                            'user_image_id' => $userProfileImage->id,
                            'attributes' => [
                                'path' => asset('storage/' . $userProfileImage->path),
                                'width' => $userProfileImage->width,
                                'height' => $userProfileImage->height,
                                'location' => $userProfileImage->location,
                            ],
                        ],
                    ],
                ],
            ]
        ]);
    }
}
