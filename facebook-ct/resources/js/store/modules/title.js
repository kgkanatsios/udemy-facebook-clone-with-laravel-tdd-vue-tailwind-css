const state = {
    title: 'Welcome',
}

const getters = {
    pageTitle: state => {
        return state.title;
    }
};

const actions = {
    setPageTitle({commit}, title){
        commit("setTitle", {title:title});
    },
};

const mutations = {
    setTitle(state,{title}){
        state.title = title + ' | KostasBook';
    }
};

export default{
    state,
    getters,
    actions,
    mutations
};
