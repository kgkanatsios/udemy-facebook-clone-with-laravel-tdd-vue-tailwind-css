const state = {
    posts: null,
    postsStatus: null,
    postMessage: null
};

const getters = {
    posts: state => {
        return state.posts;
    },
    postsStatus: state => {
        return state.postsStatus;
    },
    postMessage: state => {
        return state.postMessage;
    }
};

const actions = {
    fetchNewsPosts({ commit }) {
        commit("setPostsStatus", "loading");
        axios
            .get("/api/posts")
            .then(res => {
                commit("setPosts", res.data);
                commit("setPostsStatus", "success");
            })
            .catch(err => {
                console.log("Unable to fetch post. Error:" + err);
                commit("setPostsStatus", "error");
            });
    },
    fetchUserPosts({ commit, dispatch }, userId) {
        commit("setPostsStatus", "loading");
        axios
            .get("/api/users/" + userId + "/posts")
            .then(res => {
                commit("setPosts", res.data);
                commit("setPostsStatus", "success");
            })
            .catch(err => {
                console.log("Unable to fetch posts. Error:" + err);
                commit("setPostsStatus", "error");
            });
    },
    postMessage({ commit, getters }) {
        commit("setPostsStatus", "loading");
        axios
            .post("/api/posts", { body: getters.postMessage })
            .then(res => {
                commit("pushPost", res.data);
                commit("updateMessage", "");
                commit("setPostsStatus", "success");
            })
            .catch(err => {
                console.log("Unable to post the post. Error:" + err);
                commit("setPostsStatus", "error");
            });
    },
    likePost({ commit }, data) {
        axios
            .post("/api/posts/" + data.postId + "/like")
            .then(res => {
                commit("pushLikes", { likes: res.data, postKey: data.postKey });
            })
            .catch(err => {
                console.log("Unable to like the post. Error:" + err);
            });
    },
    commentPost({ commit }, data) {
        axios
            .post("/api/posts/" + data.postId + "/comment", { body: data.body })
            .then(res => {
                commit("pushComments", {
                    comments: res.data,
                    postKey: data.postKey
                });
            })
            .catch(err => {
                console.log("Unable to comment the post. Error:" + err);
            });
    }
};

const mutations = {
    setPosts(state, posts) {
        state.posts = posts;
    },
    setPostsStatus(state, postsStatus) {
        state.postsStatus = postsStatus;
    },
    updateMessage(state, postMessage) {
        state.postMessage = postMessage;
    },
    pushPost(state, newPost) {
        state.posts.data.unshift(newPost);
    },
    pushLikes(state, data) {
        state.posts.data[data.postKey].data.attributes.likes = data.likes;
    },
    pushComments(state, data) {
        state.posts.data[data.postKey].data.attributes.comments = data.comments;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
