const state = {
    user: null,
    userStatus: null,
}

const getters = {
    authUser: state => {
        return state.user;
    }
};

const actions = {
    fetchAuthUser({commit}) {
        axios
      .get("/api/auth-user")
      .then(res => {
        commit("setAuthUser", {user:res.data});
      })
      .catch(err => {
        console.log("Unauthedicated user. Error: " + err);
      });
    }
};

const mutations = {
    setAuthUser(state,{user}){
        state.user = user;
    }
};

export default{
    state,
    getters,
    actions,
    mutations
};
